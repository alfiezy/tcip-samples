#!/bin/bash

cd chainmaker2chainmaker
echo -e "\n" | ./clean.sh
echo -e "\n\n" | ./start_1_cross_1.sh
sleep 20

RES=$(./run_test_1_cross_1.sh)
echo $RES
RESULT1=$(echo $RES | grep "OTk5OTAuMDAwMDAwMDAwMA==")
RESULT2=$(echo $RES | grep "MTAwMDEwLjAwMDAwMDAwMDA=")

if [[ "$RESULT1" != "" ]]
then
    echo "1_cross_1 src success"
else
    echo -e "\n" | ./clean.sh
    echo "1_cross_1 src error"
    exit 1
fi

if [[ "$RESULT2" != "" ]]
then
    echo "1_cross_1 dest success"
else
    echo -e "\n" | ./clean.sh
    exit 1
fi

echo -e "\n" | ./clean.sh

echo -e "\n\n" | ./start_1_cross_2.sh
sleep 20

RES=$(./run_test_1_cross_2.sh)
echo $RES
RESULT1=$(echo $RES | grep "OTk5OTAuMDAwMDAwMDAwMA==")
RESULT2=$(echo $RES | grep "MTAwMDEwLjAwMDAwMDAwMDA=")
if [[ "$RESULT1" != "" ]]
then
    echo "1_cross_2 success"
else
    echo -e "\n" | ./clean.sh
    echo "1_cross_1 src error"
    exit 1
fi

if [[ "$RESULT2" != "" ]]
then
    echo "1_cross_2 dest success"
else
    echo -e "\n" | ./clean.sh
    echo "1_cross_1 src error"
    exit 1
fi

echo -e "\n" | ./clean.sh

exit 0