#!/bin/bash

#./cmc client contract user invoke \
#--contract-name=erc20_01 \
#--method=invoke_contract \
#--sdk-conf-path=./sdk_config_chain2_client1.yml \
#--params="{\"method\":\"transfer\",\"is_cross_chain\":\"true\",\"value\":\"10\",\"cross_chain_name\":\"first_event\",\"cross_chain_flag\":\"first_event\",\"cancel_info\":\"{\\\"chain_id\\\":\\\"chain2\\\",\\\"contract_name\\\":\\\"erc20_01\\\",\\\"method\\\":\\\"invoke_contract\\\",\\\"parameter\\\":\\\"{\\\\\\\"method\\\\\\\":\\\\\\\"minter\\\\\\\",\\\\\\\"to\\\\\\\":\\\\\\\"01ded1a81804812b7f5320bc73a258ec74e650892e841222c22405e462b670dc\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"20\\\\\\\"}\\\"}\",\"cross_chain_msgs\":\"[{\\\"gateway_id\\\": \\\"1\\\",\\\"chain_id\\\":\\\"chain3\\\",\\\"contract_name\\\":\\\"erc20_02\\\",\\\"method\\\":\\\"invoke_contract\\\",\\\"parameter\\\":\\\"{\\\\\\\"method\\\\\\\":\\\\\\\"minter\\\\\\\",\\\\\\\"to\\\\\\\":\\\\\\\"cd41f28f39e37f8dde52e622e0ea0dec811f2ad3e93c479f66325544d53028b5\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"10\\\\\\\"}\\\",\\\"confirm_info\\\":{\\\"chain_id\\\":\\\"chain3\\\",\\\"contract_name\\\":\\\"erc20_02\\\",\\\"method\\\":\\\"invoke_contract\\\",\\\"Parameter\\\":\\\"{\\\\\\\"method\\\\\\\":\\\\\\\"transfer\\\\\\\",\\\\\\\"to\\\\\\\":\\\\\\\"01ded1a81804812b7f5320bc73a258ec74e650892e841222c22405e462b670dc\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"10\\\\\\\"}\\\"},\\\"cancel_info\\\":{\\\"chain_id\\\":\\\"chain3\\\",\\\"contract_name\\\":\\\"erc20_02\\\",\\\"method\\\":\\\"invoke_contract\\\",\\\"Parameter\\\":\\\"{\\\\\\\"method\\\\\\\":\\\\\\\"burn\\\\\\\",\\\\\\\"value\\\\\\\":\\\\\\\"10\\\\\\\"}\\\"},\\\"extra_data\\\":\\\"按需写，目标网关能解析就行\\\"}]\"}" \
#--sync-result=true

cd ../fabric-samples/test-network
#在fabric-samples/test-network下调用合约的方法:
export FABRIC_CFG_PATH=$PWD/../config/
export CORE_PEER_TLS_ENABLED=true
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
export CORE_PEER_ADDRESS=localhost:7051
../bin/peer chaincode invoke \
-o localhost:7050 \
--ordererTLSHostnameOverride orderer.example.com --tls --cafile ${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem \
-C mychannel \
-n crosschain1 \
--peerAddresses localhost:7051 \
--tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt \
--peerAddresses localhost:9051 --tlsRootCertFiles ${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0.org2.example.com/tls/ca.crt -c \
'{"function":"CrossChainSave","Args":["fabr2bcoskey","fabr2bcosvalue"]}'