/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

// SaveContract provides functions for save data
type CrossChain struct {
	contractapi.Contract
}

// InitLedger adds a base set of cars to the ledger
func (c *CrossChain) InitLedger(ctx contractapi.TransactionContextInterface) error {
	return nil
}

// 触发跨链保存
func (c *CrossChain) CrossChainSave(ctx contractapi.TransactionContextInterface, key, value string) (string, error) {
	//保存key
	_ = ctx.GetStub().PutState(key, []byte(value))
	//标记跨链好了没
	_ = ctx.GetStub().PutState(key+"ready", []byte("false"))

	event := fmt.Sprintf("[\"%s\",\"%s\"]", key, value)
	err := ctx.GetStub().SetEvent("test", []byte(event))
	return "success", err
}

func (c *CrossChain) CrossChainTry(ctx contractapi.TransactionContextInterface, key, value string) (string, error) {
	// 保存
	_ = ctx.GetStub().PutState(key, []byte(value))
	//标记跨链好了没
	_ = ctx.GetStub().PutState(key+"ready", []byte("false"))
	return "success", nil
}

func (c *CrossChain) CrossChainConfirm(ctx contractapi.TransactionContextInterface, key string) (string, error) {
	// 标记跨链完成了
	_ = ctx.GetStub().PutState(key+"ready", []byte("true"))
	return "success", nil
}

func (c *CrossChain) CrossChainCancel(ctx contractapi.TransactionContextInterface, key string) (string, error) {
	// 标记跨链失败了
	_ = ctx.GetStub().PutState(key+"ready", []byte("failed"))
	return "success", nil
}

func (c *CrossChain) Query(ctx contractapi.TransactionContextInterface, key string) (string, error) {
	value, err := ctx.GetStub().GetState(key)
	if err != nil {
		return err.Error(), nil
	}
	ready, err := ctx.GetStub().GetState(key + "ready")
	if err != nil {
		return err.Error(), nil
	}
	return fmt.Sprintf("value %s, ready: %s", value, ready), nil
}

func main() {

	chaincode, err := contractapi.NewChaincode(new(CrossChain))

	if err != nil {
		fmt.Printf("Error create test chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting test chaincode: %s", err.Error())
	}
}
