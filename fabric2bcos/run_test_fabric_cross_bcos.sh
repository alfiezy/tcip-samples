#!/bin/bash

cd tcip-fabric-1-run

./invoke_1_cross_1.sh

sleep 10

./query_fabric_cross_bcos.sh

cd ../bcos-run

ADDRESS1=$(cat ./address | sed -n "1p")

cd query
./main $ADDRESS1 fabr2bcoskey

curl -k -X GET https://localhost:19999/v1/QueryCrossChain\?version=0\&page_size=10\&page_number=1