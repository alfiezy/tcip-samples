#!/bin/bash

cd bcos-run

ADDRESS1=$(cat ./address | sed -n "1p")

cd invoke
./main $ADDRESS1 bcos2fabkey bcos2fabvalue

sleep 10

cd ../query
./main $ADDRESS1 bcos2fabkey

cd ../../tcip-fabric-1-run
./query_bcos_cross_fabric.sh

curl -k -X GET https://localhost:19999/v1/QueryCrossChain\?version=0\&page_size=10\&page_number=1