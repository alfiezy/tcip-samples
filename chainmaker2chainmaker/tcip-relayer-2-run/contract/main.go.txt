/*
 Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
   SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"

	"chainmaker.org/chainmaker/tcip-go/common"

	"chainmaker.org/chainmaker/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker/chainmaker-contract-sdk-docker-go/shim"
	"github.com/gogo/protobuf/proto"
)

const (
	lastGatewayIdKey          = "lgi"
	lastCrossChainIdKey       = "lcc"
	notEndCrossChainIdListKey = "nnchil"
	failedCrossChainId        = "fcci"
)

type CrossChainManager struct{}

// 初始化合约，初始化网关id和跨链id
func (c *CrossChainManager) InitContract(stub shim.CMStubInterface) protogo.Response {
	err := stub.PutState(lastGatewayIdKey, lastGatewayIdKey, "0")
	if err != nil {
		return shim.Error("fail to init last gateway id")
	}
	err = stub.PutState(lastCrossChainIdKey, lastCrossChainIdKey, "0")
	if err != nil {
		return shim.Error("fail to init last cross chain id")
	}
	err = stub.PutStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey, []byte("[]"))
	if err != nil {
		return shim.Error("fail to init not end cross chain id list")
	}
	return shim.Success([]byte("Init Success"))
}

// 调用合约
func (c *CrossChainManager) InvokeContract(stub shim.CMStubInterface) protogo.Response {
	// 获取参数
	method := string(stub.GetArgs()["method"])

	switch method {
	case "save_gateway":
		return c.saveGateway(stub)
	case "update_gateway":
		return c.updateGateway(stub)
	case "save_cross_chain_info":
		return c.saveCrossChainInfo(stub)
	case "get_error_cross_chain_tx_list":
		return c.getErrorCrossChainTxList(stub)
	case "delete_error_cross_chain_tx_list":
		return c.deleteErrorCrossChainTxList(stub)
	case "update_cross_chain_try":
		return c.updateCrossChainTry(stub)
	case "update_cross_chain_result":
		return c.updateCrossChainResult(stub)
	case "update_cross_chain_confirm":
		return c.updateCrossChainConfirm(stub)
	case "update_src_gateway_confirm":
		return c.updateSrcGatewayConfirm(stub)
	case "get_gateway_num":
		return c.getGatewayNum(stub)
	case "get_gateway":
		return c.getGateway(stub)
	case "get_gateway_by_range":
		return c.getGatewayByRange(stub)
	case "get_cross_chain_num":
		return c.getCrossChainNum(stub)
	case "get_cross_chain_info":
		return c.getCrossChainInfo(stub)
	case "get_cross_chain_info_by_range":
		return c.getCrossChainInfoByRange(stub)
	case "get_not_end_cross_chian_id_list":
		return c.getNotEndCrossChainIdList(stub)
	default:
		return shim.Error("invalid method")
	}
}

// 保存gateway
func (c *CrossChainManager) saveGateway(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	gatewayInfoByte := params["gateway_info_byte"]
	if gatewayInfoByte == nil {
		return shim.Error("fail to get  gateway_info_byte fact bytes")
	}

	gatewayInfoByte = []byte(base64.StdEncoding.EncodeToString(gatewayInfoByte))

	// 根据最后一个gatewayId生成新的gatewayId
	lastGatewayId, err := stub.GetState(lastGatewayIdKey, lastGatewayIdKey)
	lastGatewayIdNum, err := strconv.Atoi(lastGatewayId)
	if err != nil {
		return shim.Error("get last gateway id error：" + err.Error())
	}
	gatewayId := lastGatewayIdNum

	// 保存最新的gatewayId
	err = stub.PutState(lastGatewayIdKey, lastGatewayIdKey, fmt.Sprintf("%d", lastGatewayIdNum+1))
	if err != nil {
		return shim.Error("fail to save last gateway id")
	}

	// 保存gateway
	err = stub.PutStateByte(parseGatewayKey(gatewayId), parseGatewayKey(gatewayId), gatewayInfoByte)
	if err != nil {
		return shim.Error("fail to save gateway info")
	}
	return shim.Success([]byte(lastGatewayId))
}

// 更新gateway
func (c *CrossChainManager) updateGateway(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	gatewayId := string(params["gateway_id"])
	gatewayInfoByte := params["gateway_info_byte"]

	gatewayInfoByte = []byte(base64.StdEncoding.EncodeToString(gatewayInfoByte))

	// 根据最后一个gatewayId判断用户传的gatewayId是否存在
	lastGatewayId, err := stub.GetState(lastGatewayIdKey, lastGatewayIdKey)
	if err != nil {
		return shim.Error("get last gateway id error：" + err.Error())
	}
	lastGatewayIdNum, err := strconv.Atoi(lastGatewayId)
	if err != nil {
		return shim.Error("get last gateway id error：" + err.Error())
	}
	gatewayIdNum, err := strconv.Atoi(gatewayId)
	if err != nil || gatewayIdNum >= lastGatewayIdNum {
		return shim.Error("invalid gateway_id: " + gatewayId)
	}

	// 保存gateway
	err = stub.PutStateByte(parseGatewayKey(gatewayIdNum), parseGatewayKey(gatewayIdNum), gatewayInfoByte)
	if err != nil {
		return shim.Error("fail to save gateway info")
	}
	return shim.Success([]byte(gatewayId))
}

func (c *CrossChainManager) getGatewayNum(stub shim.CMStubInterface) protogo.Response {
	lastGatewayId, err := stub.GetState(lastGatewayIdKey, lastGatewayIdKey)
	if err != nil {
		return shim.Error("get last gateway id error：" + err.Error())
	}
	return shim.Success([]byte(lastGatewayId))
}

func (c *CrossChainManager) getGateway(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	gatewayId := string(params["gateway_id"])
	gatewayIdNum, err := strconv.Atoi(gatewayId)
	if err != nil {
		return shim.Error("get gateway error：" + err.Error())
	}

	gatewayInfoByte, err := stub.GetStateByte(parseGatewayKey(gatewayIdNum), parseGatewayKey(gatewayIdNum))
	if err != nil {
		return shim.Error("no such gateway id:" + gatewayId)
	}
	gatewayInfoDecode, err := base64.StdEncoding.DecodeString(string(gatewayInfoByte))
	if err != nil {
		return shim.Error("decode gateway error: " + err.Error())
	}
	return shim.Success(gatewayInfoDecode)
}

func (c *CrossChainManager) getGatewayByRange(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	startGatewayId := string(params["start_gateway_id"])
	stopGatewayId := string(params["stop_gateway_id"])

	startGatewayIdNum, err := strconv.Atoi(startGatewayId)
	if err != nil {
		return shim.Error("get gateway error：" + err.Error())
	}
	stopGatewayIdNum, err := strconv.Atoi(stopGatewayId)
	if err != nil {
		return shim.Error("get gateway error：" + err.Error())
	}
	result, err := stub.NewIterator(parseGatewayKey(startGatewayIdNum), parseGatewayKey(stopGatewayIdNum))
	if err != nil {
		return shim.Error("get gateway by range error: " + err.Error())
	}

	var gatewayInfos [][]byte

	for result.HasNext() {
		_, _, gatewayInfo, err := result.Next()
		if err != nil {
			return shim.Error("get gateway from iterator error: " + err.Error())
		}
		gatewayInfoDecode, err := base64.StdEncoding.DecodeString(string(gatewayInfo))
		if err != nil {
			return shim.Error("decode gateway error: " + err.Error())
		}
		gatewayInfos = append(gatewayInfos, gatewayInfoDecode)
	}
	shim.Logger.Error(fmt.Sprintf("result len %d", len(gatewayInfos)))
	resultByte, err := json.Marshal(gatewayInfos)
	if err != nil {
		return shim.Error("get gateway slice byte error: " + err.Error())
	}
	return shim.Success(resultByte)
}

func (c *CrossChainManager) saveCrossChainInfo(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainInfoByte := params["cross_chain_info_byte"]
	if crossChainInfoByte == nil {
		return shim.Error("fail to get cross chain bytes")
	}
	var crossChainInfo common.CrossChainInfo
	_ = json.Unmarshal(crossChainInfoByte, &crossChainInfo)

	// 根据最后一个crossChainId生成新的crossChainId
	lastCrossChainId, err := stub.GetState(lastCrossChainIdKey, lastCrossChainIdKey)
	lastCrossChainIdNum, err := strconv.Atoi(lastCrossChainId)
	if err != nil {
		return shim.Error("get last cross chain id error：" + err.Error())
	}
	crossChainId := lastCrossChainIdNum
	crossChainStr := lastCrossChainId

	crossChainInfo.CrossChainId = crossChainStr
	crossChainInfo.State = common.CrossChainStateValue_WAIT_EXECUTE

	crossChainInfoByte, _ = json.Marshal(&crossChainInfo)
	crossChainInfoByte = []byte(base64.StdEncoding.EncodeToString(crossChainInfoByte))

	// 保存最新的crossChainId
	err = stub.PutState(lastCrossChainIdKey, lastCrossChainIdKey, fmt.Sprintf("%d", lastCrossChainIdNum+1))
	if err != nil {
		return shim.Error("fail to save last cross chain id")
	}

	// 保存crossChain
	err = stub.PutStateByte(parseCrossChainKey(crossChainId), parseCrossChainKey(crossChainId), crossChainInfoByte)
	if err != nil {
		return shim.Error("fail to save cross chain info")
	}

	// 获取未完成的cross chain id
	notEndCrossChainIdListByte, err := stub.GetStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey)
	if err != nil {
		return shim.Error("get not end chain id list error：" + err.Error())
	}
	var notEndCrossChainIdList []string
	err = json.Unmarshal(notEndCrossChainIdListByte, &notEndCrossChainIdList)
	if err != nil {
		return shim.Error("unmarshal not end chain id list error：" + err.Error())
	}
	isExist := false
	for _, v := range notEndCrossChainIdList {
		if v == crossChainStr {
			isExist = true
			break
		}
	}
	if !isExist {
		notEndCrossChainIdList = append(notEndCrossChainIdList, crossChainStr)
		notEndCrossChainIdListByte, err = json.Marshal(notEndCrossChainIdList)
		if err != nil {
			return shim.Error("marshal not end chain id list error：" + err.Error())
		}
		err = stub.PutStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey, notEndCrossChainIdListByte)
		if err != nil {
			return shim.Error("fail to update not end cross chain id list")
		}
	}

	stub.EmitEvent(common.EventName_NEW_CROSS_CHAIN.String(), []string{crossChainStr})
	return shim.Success([]byte(crossChainStr))
}

func (c *CrossChainManager) updateCrossChainTry(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainId := string(params["cross_chain_id"])
	crossChainTryByte := params["cross_chain_tx_byte"]

	oldCrossChainInfo, err := getCrossChainInfo(crossChainId, stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	var crossChainTxUpChain []*common.CrossChainTxUpChain
	err = json.Unmarshal(crossChainTryByte, &crossChainTxUpChain)
	if err != nil {
		return shim.Error("Unmarshal cross_chain_info_byte failed" + err.Error())
	}
	if len(oldCrossChainInfo.CrossChainTxContent) == 0 {
		oldCrossChainInfo.CrossChainTxContent = make([]*common.TxContentWithVerify,
			len(oldCrossChainInfo.CrossChainMsg))
	}
	hasNil := false
	for _, crossChainTxContent := range crossChainTxUpChain {
		if crossChainTxContent != nil {
			oldCrossChainInfo.CrossChainTxContent[crossChainTxContent.Index] = crossChainTxContent.TxContentWithVerify
		}
	}
	for _, crossChainTxContent := range oldCrossChainInfo.CrossChainTxContent {
		if crossChainTxContent == nil {
			hasNil = true
		}
	}
	if !hasNil {
		oldCrossChainInfo.State = common.CrossChainStateValue_WAIT_CONFIRM
		stub.EmitEvent(common.EventName_CROSS_CHAIN_TRY_END.String(), []string{oldCrossChainInfo.CrossChainId})
	}
	crossChainInfoByte, _ := json.Marshal(oldCrossChainInfo)
	crossChainInfoByte = []byte(base64.StdEncoding.EncodeToString(crossChainInfoByte))

	crossChainIdNum, err := strconv.Atoi(oldCrossChainInfo.CrossChainId)
	if err != nil {
		return shim.Error("CrossChainId is not number：" + err.Error())
	}
	// 保存cross chain
	err = stub.PutStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum), crossChainInfoByte)
	if err != nil {
		return shim.Error("fail to save cross chain info")
	}
	return shim.Success([]byte(oldCrossChainInfo.CrossChainId))
}

func (c *CrossChainManager) updateCrossChainResult(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainId := string(params["cross_chain_id"])
	crossChainResult := string(params["cross_chain_result"])

	oldCrossChainInfo, err := getCrossChainInfo(crossChainId, stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	crossChainIdNum, err := strconv.Atoi(oldCrossChainInfo.CrossChainId)
	if err != nil {
		return shim.Error("CrossChainId is not number：" + err.Error())
	}

	if crossChainResult == "true" {
		oldCrossChainInfo.CrossChainResult = true
	} else {
		oldCrossChainInfo.CrossChainResult = false
		failedCrossChainIdKey := parseFailCrossChainIdKey(crossChainIdNum)
		// 保存失败的crossChainId
		err = stub.PutStateByte(failedCrossChainIdKey, failedCrossChainIdKey, []byte(oldCrossChainInfo.CrossChainId))
		if err != nil {
			return shim.Error("fail to save cross chain info")
		}
	}
	stub.EmitEvent(common.EventName_UPADATE_RESULT_END.String(), []string{oldCrossChainInfo.CrossChainId})
	crossChainInfoByte, _ := json.Marshal(oldCrossChainInfo)
	crossChainInfoByte = []byte(base64.StdEncoding.EncodeToString(crossChainInfoByte))
	// 保存cross chain
	err = stub.PutStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum), crossChainInfoByte)
	if err != nil {
		return shim.Error("fail to save cross chain info")
	}

	return shim.Success([]byte(oldCrossChainInfo.CrossChainId))
}

func (c *CrossChainManager) getErrorCrossChainTxList(stub shim.CMStubInterface) protogo.Response {
	result, err := stub.NewIterator(parseFailCrossChainIdKey(0),
		parseFailCrossChainIdKey(1 << 19 - 1))
	if err != nil {
		return shim.Error("get gateway by range error: " + err.Error())
	}
	ids := make([][]byte, 0)

	for result.HasNext() {
		_, _, id, err := result.Next()
		if err != nil {
			return shim.Error("getErrorCrossChainTxList from iterator error: " + err.Error())
		}
		ids = append(ids, id)
	}
	idsByte, _ := json.Marshal(ids)
	return shim.Success(idsByte)
}

func (c *CrossChainManager) deleteErrorCrossChainTxList(stub shim.CMStubInterface) protogo.Response {
	crossChainId := string(stub.GetArgs()["cross_chain_id"])
	crossChainIdNum, err := strconv.Atoi(crossChainId)
	if err != nil {
		return shim.Error("CrossChainId is not number：" + err.Error())
	}
	_ = stub.DelState(parseFailCrossChainIdKey(crossChainIdNum), parseFailCrossChainIdKey(crossChainIdNum))
	return shim.Success([]byte("success"))
}

func (c *CrossChainManager) updateCrossChainConfirm(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainId := string(params["cross_chain_id"])
	crossChainConfirmByte := params["cross_chain_confirm_byte"]

	oldCrossChainInfo, err := getCrossChainInfo(crossChainId, stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	var crossChainConfirm []*common.CrossChainConfirmUpChain
	err = json.Unmarshal(crossChainConfirmByte, &crossChainConfirm)
	if err != nil {
		return shim.Error("Unmarshal cross_chain_info_byte failed" + err.Error())
	}
	if len(oldCrossChainInfo.GatewayConfirmResult) == 0 {
		oldCrossChainInfo.GatewayConfirmResult = make([]*common.CrossChainConfirm,
			len(oldCrossChainInfo.CrossChainMsg))
	}
	hasNil := false
	for _, gatewayConfirmResult := range crossChainConfirm {
		if gatewayConfirmResult != nil {
			oldCrossChainInfo.GatewayConfirmResult[gatewayConfirmResult.Index] = gatewayConfirmResult.CrossChainConfirm
		}
	}
	for _, gatewayConfirmResult := range oldCrossChainInfo.GatewayConfirmResult {
		if gatewayConfirmResult == nil {
			hasNil = true
			break
		}
	}
	if !hasNil {
		stub.EmitEvent(common.EventName_GATEWAY_CONFIRM_END.String(), []string{oldCrossChainInfo.CrossChainId})
	}
	crossChainInfoByte, _ := json.Marshal(oldCrossChainInfo)
	crossChainInfoByte = []byte(base64.StdEncoding.EncodeToString(crossChainInfoByte))

	crossChainIdNum, err := strconv.Atoi(oldCrossChainInfo.CrossChainId)
	if err != nil {
		return shim.Error("CrossChainId is not number：" + err.Error())
	}
	// 保存cross chain
	err = stub.PutStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum), crossChainInfoByte)
	if err != nil {
		return shim.Error("fail to save cross chain info")
	}
	return shim.Success([]byte(oldCrossChainInfo.CrossChainId))
}

func (c *CrossChainManager) updateSrcGatewayConfirm(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainId := string(params["cross_chain_id"])
	confrimResultByte := params["confirm_result"]

	oldCrossChainInfo, err := getCrossChainInfo(crossChainId, stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	var confrimResult common.CrossChainConfirm
	err = proto.Unmarshal(confrimResultByte, &confrimResult)
	if err != nil {
		return shim.Error("Unmarshal confrimeResult failed" + err.Error())
	}

	if oldCrossChainInfo.ConfirmResult != nil && *oldCrossChainInfo.ConfirmResult != (common.CrossChainConfirm{}) {
		return shim.Success([]byte("It's been updated"))
	}
	oldCrossChainInfo.ConfirmResult = &confrimResult
	stub.EmitEvent(common.EventName_SRC_GATEWAY_CONFIRM_END.String(), []string{oldCrossChainInfo.CrossChainId})
	if oldCrossChainInfo.CrossChainResult {
		oldCrossChainInfo.State = common.CrossChainStateValue_CONFIRM_END
	} else {
		oldCrossChainInfo.State = common.CrossChainStateValue_CANCEL_END
	}
	crossChainInfoByte, _ := json.Marshal(oldCrossChainInfo)
	crossChainInfoByte = []byte(base64.StdEncoding.EncodeToString(crossChainInfoByte))

	crossChainIdNum, err := strconv.Atoi(oldCrossChainInfo.CrossChainId)
	if err != nil {
		return shim.Error("CrossChainId is not number：" + err.Error())
	}
	// 保存cross chain
	err = stub.PutStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum), crossChainInfoByte)
	if err != nil {
		return shim.Error("fail to save cross chain info")
	}

	// 从未完成的列表中删除
	notEndCrossChainIdListByte, err := stub.GetStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey)
	if err != nil {
		return shim.Error("get not end chain id list error：" + err.Error())
	}
	var notEndCrossChainIdList []string
	err = json.Unmarshal(notEndCrossChainIdListByte, &notEndCrossChainIdList)
	if err != nil {
		return shim.Error("unmarshal not end chain id list error：" + err.Error())
	}
	flag := -1
	for i, v := range notEndCrossChainIdList {
		if v == crossChainId {
			flag = i
			break
		}
	}
	if flag != -1 {
		notEndCrossChainIdList = append(notEndCrossChainIdList[:flag], notEndCrossChainIdList[flag+1:]...)
		notEndCrossChainIdListByte, err = json.Marshal(notEndCrossChainIdList)
		if err != nil {
			return shim.Error("marshal not end chain id list error：" + err.Error())
		}
		err = stub.PutStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey, notEndCrossChainIdListByte)
		if err != nil {
			return shim.Error("fail to update not end cross chain id list")
		}
	}
	return shim.Success([]byte(oldCrossChainInfo.CrossChainId))
}

func (c *CrossChainManager) getCrossChainNum(stub shim.CMStubInterface) protogo.Response {
	lastCrossChainId, err := stub.GetState(lastCrossChainIdKey, lastCrossChainIdKey)
	if err != nil {
		return shim.Error("get last cross chain id error：" + err.Error())
	}
	return shim.Success([]byte(lastCrossChainId))
}

func (c *CrossChainManager) getCrossChainInfo(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	crossChainId := string(params["cross_chain_id"])

	crossChainIdNum, err := strconv.Atoi(crossChainId)
	if err != nil {
		return shim.Error("get cross chain info error：" + err.Error())
	}

	crossChainInfoByte, err := stub.GetStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum))
	if err != nil {
		return shim.Error("no such cross chain id:" + crossChainId)
	}
	crossChainInfoDecode, err := base64.StdEncoding.DecodeString(string(crossChainInfoByte))
	if err != nil {
		return shim.Error("decode cross chain error: " + err.Error())
	}
	return shim.Success(crossChainInfoDecode)
}

func getCrossChainInfo(crossChainId string, stub shim.CMStubInterface) (*common.CrossChainInfo, error) {

	crossChainIdNum, err := strconv.Atoi(crossChainId)
	if err != nil {
		return nil, errors.New("get cross chain info error：" + err.Error())
	}

	crossChainInfoByte, err := stub.GetStateByte(parseCrossChainKey(crossChainIdNum), parseCrossChainKey(crossChainIdNum))
	if err != nil {
		return nil, errors.New("no such cross chain id:" + crossChainId)
	}
	crossChainInfoDecode, err := base64.StdEncoding.DecodeString(string(crossChainInfoByte))
	if err != nil {
		return nil, errors.New("decode cross chain error: " + err.Error())
	}
	var crossChainInfo common.CrossChainInfo
	err = json.Unmarshal(crossChainInfoDecode, &crossChainInfo)
	if err != nil {
		return nil, errors.New("unmarshal cross chain error: " + err.Error())
	}
	return &crossChainInfo, nil
}

func (c *CrossChainManager) getCrossChainInfoByRange(stub shim.CMStubInterface) protogo.Response {
	params := stub.GetArgs()

	// 获取参数
	startCrossChainId := string(params["start_cross_chain_id"])
	stopCrossChainId := string(params["stop_cross_chain_id"])

	startCrossChainIdNum, err := strconv.Atoi(startCrossChainId)
	if err != nil {
		return shim.Error("get cross chain info error：" + err.Error())
	}
	stopCrossChainIdNum, err := strconv.Atoi(stopCrossChainId)
	if err != nil {
		return shim.Error("get cross chain info error：" + err.Error())
	}
	result, err := stub.NewIterator(parseCrossChainKey(startCrossChainIdNum), parseCrossChainKey(stopCrossChainIdNum))
	if err != nil {
		return shim.Error("get cross chain by range error: " + err.Error())
	}

	crossChainInfos := make([][]byte, 0)

	for result.HasNext() {
		_, _, crossChainInfo, err := result.Next()
		if err != nil {
			return shim.Error("get cross chain from iterator error: " + err.Error())
		}
		crossChainInfoDecode, err := base64.StdEncoding.DecodeString(string(crossChainInfo))
		if err != nil {
			return shim.Error("decode cross chain error: " + err.Error())
		}
		crossChainInfos = append(crossChainInfos, crossChainInfoDecode)
	}
	//crossChainInfos = crossChainInfos[0:len(crossChainInfos)-3]
	resultByte, err := json.Marshal(crossChainInfos)
	if err != nil {
		return shim.Error("get cross chain slice byte error: " + err.Error())
	}
	return shim.Success(resultByte)
}

func (c *CrossChainManager) getNotEndCrossChainIdList(stub shim.CMStubInterface) protogo.Response {
	notEndCrossChainIdListByte, err := stub.GetStateByte(notEndCrossChainIdListKey, notEndCrossChainIdListKey)
	if err != nil {
		return shim.Error("get not end chain id list error：" + err.Error())
	}
	return shim.Success(notEndCrossChainIdListByte)
}

func parseGatewayKey(gatewayId int) string {
	return fmt.Sprintf("g%019d", gatewayId)
}

func parseCrossChainKey(crossChainId int) string {
	return fmt.Sprintf("c%019d", crossChainId)
}

func parseFailCrossChainIdKey(crossChainId int) string {
	return failedCrossChainId + parseCrossChainKey(crossChainId)
}

func main() {
	err := shim.Start(new(CrossChainManager))
	if err != nil {
		log.Fatal(err)
	}
}
