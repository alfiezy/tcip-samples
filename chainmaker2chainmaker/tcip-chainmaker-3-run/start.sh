#!/bin/bash
./tcip-chainmaker register -c config/tcip_chainmaker.yml

REGISTER=$(cat register.json)
curl -k -H "Content-Type: application/json" -X POST -d "$REGISTER" https://localhost:20000/v1/GatewayRegister

./tcip-chainmaker spv -c config/tcip_chainmaker.yml \
-v 1.0 \
-p ./contract/spv.7z \
-r DOCKER_GO \
-P "{}" \
-C chainmaker003 \
-O install

nohup ./tcip-chainmaker start -c ./config/tcip_chainmaker.yml > panic.log 2>&1 & echo $! > pid

sleep 5

CHAINCONFIG=$(cat chain_config.json)
curl -k -H "Content-Type: application/json" -X POST -d "$CHAINCONFIG" https://localhost:19998/v1/ChainIdentity

echo "tcip-chainmaker-3 start"