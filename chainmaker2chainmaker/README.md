# tcip-samples使用文档

>执行部署脚本时需要注意，如果之前执行过一次部署脚本，那么需要先执行clean.sh脚本进行环境清理

## 1. 环境部署

### 1.1 准备工作

&emsp;&emsp;&emsp;&emsp;在开始部署环境之前，首先需要安装7z和docker，

&emsp;&emsp;&emsp;&emsp;7z安装参考[7zip官网](https://sparanoid.com/lab/7z/),

&emsp;&emsp;&emsp;&emsp;docker安装参考[https://docs.docker.com/engine/install/](https://docs.docker.com/engine/install/)

&emsp;&emsp;&emsp;&emsp;此外还需要拉去Docker VM官方镜像：
```shell
docker pull chainmakerofficial/chainmaker-vm-engine:v2.3.0
```

### 1.2 环境部署

![网关部署结构](./环境架构.png)

####一对一中心化中继网关跨链环境部署(对应上图组织2内部跨链交易)
```shell
./start_1_cross_1.sh
```
&emsp;&emsp;&emsp;&emsp;执行此脚本之后，会自动启动一个solo模式的chainmaker节点，以及三条链，chain1（中继链）、chain2（业务链1）、chain3（业务链2），一个tcip-relayer服务，两个tcip-chainmaker服务，并自动执行初始化（部署spv合约和跨链合约）。

####一对二分布式中继网关跨链环境部署(对应上图组织一与组织2进行跨链交易)
```shell
./start_1_cross_2.sh
```

&emsp;&emsp;&emsp;&emsp;执行此脚本之后，会自动启动一个solo模式的chainmaker节点，以及四条链，chain1（中继链）、chain2（业务链1）、chain3（业务链2），chain4（业务链3），两个tcip-relayer服务，三个tcip-chainmaker服务，并自动执行初始化。

### 1.3 服务停止
```shell
./stop.sh
```

&emsp;&emsp;&emsp;&emsp;执行此脚本之后,会停止所有服务，但不会删除数据

### 1.3 服务清理
```shell
./clean.sh
```

&emsp;&emsp;&emsp;&emsp;执行此脚本之后,会停止所有服务，并删除所有数据和日志


## 2. 测试

### 2.1 执行脚本

&emsp;&emsp;&emsp;&emsp;执行如下脚本进行测试,该脚本可以发送一次跨链转账交易，并查询一次跨链交易列表

```shell
# 一对一跨链测试脚本
./run_test_1_cross_1.sh
```

```shell
# 一对二跨链测试脚本
./run_test_1_cross_2.sh
```

### 2.2 单步执行

#### 2.2.1 发起交易

&emsp;&emsp;&emsp;&emsp;首先需要对tcip-chainmaker-1对应的链chain2，发起一笔到tcip-chainmaker-2对应的链chain3的跨链转账交易请求

```shell
cd chainmaker-go-run

# 一对一跨链
./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2_client1.yml \
--params="{\"method\":\"transfer\",\"to\":\"lockaddress\",\"value\":\"10\"}" \
--sync-result=true

# 一对二跨链
./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2_client1.yml \
--params="{\"method\":\"transfer\",\"to\":\"lockaddress\",\"value\":\"10\"}" \
--sync-result=true
```

&emsp;&emsp;&emsp;&emsp;收到类似如下消息，表明交易发送成功
```shell
{
  "contract_result": {
    "contract_event": [
      {
        "contract_name": "erc20_01",
        "event_data": [
          "69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb",
          "lockaddress",
          "10"
        ],
        "topic": "transfer",
        "tx_id": "17094b3bd830e929ca4033ba5f5872d46401f63ae3d74cb496bba37d8b3d1689"
      }
    ],
    "gas_used": 15040,
    "message": "Success"
  },
  "tx_block_height": 5,
  "tx_id": "17094b3bd830e929ca4033ba5f5872d46401f63ae3d74cb496bba37d8b3d1689",
  "tx_timestamp": 1659940658
}
```

#### 2.2.2 查询交易结果

&emsp;&emsp;&emsp;&emsp;2、为保证跨链交易执行完成，需要等待10秒钟，然后对chain2和chain3中的余额发起查询请求

```shell
cd chainmaker-go-run

# 一对一跨链
./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true

./cmc client contract user invoke \
--contract-name=erc20_02 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain3_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true

# 一对二跨链
./cmc client contract user invoke \
--contract-name=erc20_01 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain2_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true

./cmc client contract user invoke \
--contract-name=erc20_02 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain3_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true

./cmc client contract user invoke \
--contract-name=erc20_03 \
--method=invoke_contract \
--sdk-conf-path=./sdk_config_chain4_client1.yml \
--params="{\"method\":\"balanceOf\",\"name\":\"69cb09210603496890a5d9423f1041ef7c985aa962c0f76e8f985d2efbacb1bb\"}" \
--sync-result=true
```

&emsp;&emsp;&emsp;&emsp;跨链合约在初始化的时候，给两条链中的账户中都初始化了100000的余额，进行一次跨链交易，chain2中的账户会少10块钱，chain3中的账户会多10块钱，收到类似如下返回，表明跨链转账成功

```shell
# 一对一跨链
{
  "contract_result": {
    "gas_used": 11266,
    "message": "Success",
    "result": "OTk5OTAuMDAwMDAwMDAwMA=="
  },
  "tx_block_height": 6,
  "tx_id": "17094b3e4e382dbbcad62b124f9629ae08681cc5394344299ec783fc0b72aa18",
  "tx_timestamp": 1659940668
}
{
  "contract_result": {
    "gas_used": 11267,
    "message": "Success",
    "result": "MTAwMDEwLjAwMDAwMDAwMDA="
  },
  "tx_block_height": 7,
  "tx_id": "17094b3eabdc300eca1b64c1d69d7fbecdc56a81ba5f408dacb8799eea1566f6",
  "tx_timestamp": 1659940670
}

# 一对二跨链
{
  "contract_result": {
    "gas_used": 11266,
    "message": "Success",
    "result": "OTk5OTAuMDAwMDAwMDAwMA=="
  },
  "tx_block_height": 6,
  "tx_id": "17094b3e4e382dbbcad62b124f9629ae08681cc5394344299ec783fc0b72aa18",
  "tx_timestamp": 1659940668
}
{
  "contract_result": {
    "gas_used": 11267,
    "message": "Success",
    "result": "MTAwMDEwLjAwMDAwMDAwMDA="
  },
  "tx_block_height": 7,
  "tx_id": "17094b3eabdc300eca1b64c1d69d7fbecdc56a81ba5f408dacb8799eea1566f6",
  "tx_timestamp": 1659940670
}
{
  "contract_result": {
    "gas_used": 11267,
    "message": "Success",
    "result": "MTAwMDEwLjAwMDAwMDAwMDA="
  },
  "tx_block_height": 7,
  "tx_id": "17094b3f09bdf864ca44c4b713c17231f2fbbda67d3e42ac9996507e1fb3fdf6",
  "tx_timestamp": 1659940671
}
```

#### 2.2.3 查询跨链交易

&emsp;&emsp;&emsp;&emsp;执行如下命令，向tcip-relay服务发起查询跨链交易请求

| 参数           | 描述          |
| :------------ | :------------ |
| version       | 跨链版本，固定为0|
| page_size     | 每页多少条     |
| page_number   | 第几页        |

```shell
curl -k -X GET https://localhost:19999/v1/QuerCrossChain\?version=0\&page_size=10\&page_number=1
```

&emsp;&emsp;&emsp;&emsp;查看返回的跨链交易中的cross_chain_result字段，如果为true，表明此次跨链交易成功

```shell
{"cross_chain_info":[{"cross_chain_id":"0","cross_chain_name":"first_event","cross_chain_flag":"first_event","from":"0","cross_chain_msg":[{"gateway_id":"1","chain_id":"chain3","contract_name":"erc20_02","method":"invoke_contract","parameter":"{\"method\":\"minter\",\"to\":\"cd41f28f39e37f8dde52e622e0ea0dec811f2ad3e93c479f66325544d53028b5\",\"value\":\"10\"}","extra_data":"按需写，目标网关能解析就行","confirm_info":{"chain_id":"chain3","contract_name":"erc20_02","method":"invoke_contract","parameter":"{\"method\":\"transfer\",\"to\":\"01ded1a81804812b7f5320bc73a258ec74e650892e841222c22405e462b670dc\",\"value\":\"10\"}"},"cancel_info":{"chain_id":"chain3","contract_name":"erc20_02","method":"invoke_contract","parameter":"{\"method\":\"burn\",\"value\":\"10\"}"}}],"first_tx_content":{"tx_content":{"tx_id":"16fb6d5d98ccd94bca52fdfc072182656783de3116c3430cab48633972f1a611","tx":"CucICgZjaGFpbjIaQDE2ZmI2ZDVkOThjY2Q5NGJjYTUyZmRmYzA3MjE4MjY1Njc4M2RlMzExNmMzNDMwY2FiNDg2MzM5NzJmMWE2MTEgoMHUlQYyCGVyYzIwXzAxOg9pbnZva2VfY29udHJhY3RCFgoOaXNfY3Jvc3NfY2hhaW4SBHRydWVCCwoFdmFsdWUSAjEwQh8KEGNyb3NzX2NoYWluX25hbWUSC2ZpcnN0X2V2ZW50Qh8KEGNyb3NzX2NoYWluX2ZsYWcSC2ZpcnN0X2V2ZW50Qt4BCgtjYW5jZWxfaW5mbxLOAXsiY2hhaW5faWQiOiJjaGFpbjIiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDEiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJwYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcIm1pbnRlclwiLFwidG9cIjpcIjAxZGVkMWE4MTgwNDgxMmI3ZjUzMjBiYzczYTI1OGVjNzRlNjUwODkyZTg0MTIyMmMyMjQwNWU0NjJiNjcwZGNcIixcInZhbHVlXCI6XCIyMFwifSJ9Qp0FChBjcm9zc19jaGFpbl9tc2dzEogFW3siZ2F0ZXdheV9pZCI6ICIxIiwiY2hhaW5faWQiOiJjaGFpbjMiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDIiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJwYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcIm1pbnRlclwiLFwidG9cIjpcImNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjVcIixcInZhbHVlXCI6XCIxMFwifSIsImNvbmZpcm1faW5mbyI6eyJjaGFpbl9pZCI6ImNoYWluMyIsImNvbnRyYWN0X25hbWUiOiJlcmMyMF8wMiIsIm1ldGhvZCI6Imludm9rZV9jb250cmFjdCIsIlBhcmFtZXRlciI6IntcIm1ldGhvZFwiOlwidHJhbnNmZXJcIixcInRvXCI6XCIwMWRlZDFhODE4MDQ4MTJiN2Y1MzIwYmM3M2EyNThlYzc0ZTY1MDg5MmU4NDEyMjJjMjI0MDVlNDYyYjY3MGRjXCIsXCJ2YWx1ZVwiOlwiMTBcIn0ifSwiY2FuY2VsX2luZm8iOnsiY2hhaW5faWQiOiJjaGFpbjMiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDIiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJQYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcImJ1cm5cIixcInZhbHVlXCI6XCIxMFwifSJ9LCJleHRyYV9kYXRhIjoi5oyJ6ZyA5YaZ77yM55uu5qCH572R5YWz6IO96Kej5p6Q5bCx6KGMIn1dQhIKBm1ldGhvZBIIdHJhbnNmZXIShwEKOwoVd3gtb3JnLmNoYWlubWFrZXIub3JnEAEaIIo+b0nlQpOxQ2C11CGmMXdLJezkW9MMLqDBixJXG+FZEkgwRgIhAILnyAF0L93trn0yty2MvvNDYklobCu/tW7cpV1FkJKRAiEAglzrT4RbRiJN3Fdjzt594INctBLUriQsZUyQfmpWNVsi1wUSsgUSB3N1Y2Nlc3MaB1N1Y2Nlc3Mgk4sBKpkFCgR0ZXN0EkAxNmZiNmQ1ZDk4Y2NkOTRiY2E1MmZkZmMwNzIxODI2NTY3ODNkZTMxMTZjMzQzMGNhYjQ4NjMzOTcyZjFhNjExGghlcmMyMF8wMSIDMS4wKr8EGgtmaXJzdF9ldmVudCILZmlyc3RfZXZlbnQqkAMKATESBmNoYWluMxoIZXJjMjBfMDIiD2ludm9rZV9jb250cmFjdDJoeyJtZXRob2QiOiJtaW50ZXIiLCJ0byI6ImNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjUiLCJ2YWx1ZSI6IjEwIn06J+aMiemcgOWGme+8jOebruagh+e9keWFs+iDveino+aekOWwseihjEKPARIGY2hhaW4zGghlcmMyMF8wMiIPaW52b2tlX2NvbnRyYWN0Kmp7Im1ldGhvZCI6InRyYW5zZmVyIiwidG8iOiIwMWRlZDFhODE4MDQ4MTJiN2Y1MzIwYmM3M2EyNThlYzc0ZTY1MDg5MmU4NDEyMjJjMjI0MDVlNDYyYjY3MGRjIiwidmFsdWUiOiIxMCJ9SkMSBmNoYWluMxoIZXJjMjBfMDIiD2ludm9rZV9jb250cmFjdCoeeyJtZXRob2QiOiJidXJuIiwidmFsdWUiOiIxMCJ9SgBSjQESBmNoYWluMhoIZXJjMjBfMDEiD2ludm9rZV9jb250cmFjdCpoeyJtZXRob2QiOiJtaW50ZXIiLCJ0byI6IjAxZGVkMWE4MTgwNDgxMmI3ZjUzMjBiYzczYTI1OGVjNzRlNjUwODkyZTg0MTIyMmMyMjQwNWU0NjJiNjcwZGMiLCJ2YWx1ZSI6IjIwIn0aIBjZEokbj2Mtb2FFRI86CFg5J3aex93sjNPgaTcZkmHP","gateway_id":"0","chain_id":"chain2","tx_prove":"{\"hash_array\":\"WyIzOGV4dHhMVHcwNGZaZXJnVyszTUFhdXZTNXVOOW1UN0ZiOVhWODI2WTRJPSJd\",\"hash_type\":\"U0hBMjU2\",\"tx_byte\":\"Cs4PCucICgZjaGFpbjIaQDE2ZmI2ZDVkOThjY2Q5NGJjYTUyZmRmYzA3MjE4MjY1Njc4M2RlMzExNmMzNDMwY2FiNDg2MzM5NzJmMWE2MTEgoMHUlQYyCGVyYzIwXzAxOg9pbnZva2VfY29udHJhY3RCFgoOaXNfY3Jvc3NfY2hhaW4SBHRydWVCCwoFdmFsdWUSAjEwQh8KEGNyb3NzX2NoYWluX25hbWUSC2ZpcnN0X2V2ZW50Qh8KEGNyb3NzX2NoYWluX2ZsYWcSC2ZpcnN0X2V2ZW50Qt4BCgtjYW5jZWxfaW5mbxLOAXsiY2hhaW5faWQiOiJjaGFpbjIiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDEiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJwYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcIm1pbnRlclwiLFwidG9cIjpcIjAxZGVkMWE4MTgwNDgxMmI3ZjUzMjBiYzczYTI1OGVjNzRlNjUwODkyZTg0MTIyMmMyMjQwNWU0NjJiNjcwZGNcIixcInZhbHVlXCI6XCIyMFwifSJ9Qp0FChBjcm9zc19jaGFpbl9tc2dzEogFW3siZ2F0ZXdheV9pZCI6ICIxIiwiY2hhaW5faWQiOiJjaGFpbjMiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDIiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJwYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcIm1pbnRlclwiLFwidG9cIjpcImNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjVcIixcInZhbHVlXCI6XCIxMFwifSIsImNvbmZpcm1faW5mbyI6eyJjaGFpbl9pZCI6ImNoYWluMyIsImNvbnRyYWN0X25hbWUiOiJlcmMyMF8wMiIsIm1ldGhvZCI6Imludm9rZV9jb250cmFjdCIsIlBhcmFtZXRlciI6IntcIm1ldGhvZFwiOlwidHJhbnNmZXJcIixcInRvXCI6XCIwMWRlZDFhODE4MDQ4MTJiN2Y1MzIwYmM3M2EyNThlYzc0ZTY1MDg5MmU4NDEyMjJjMjI0MDVlNDYyYjY3MGRjXCIsXCJ2YWx1ZVwiOlwiMTBcIn0ifSwiY2FuY2VsX2luZm8iOnsiY2hhaW5faWQiOiJjaGFpbjMiLCJjb250cmFjdF9uYW1lIjoiZXJjMjBfMDIiLCJtZXRob2QiOiJpbnZva2VfY29udHJhY3QiLCJQYXJhbWV0ZXIiOiJ7XCJtZXRob2RcIjpcImJ1cm5cIixcInZhbHVlXCI6XCIxMFwifSJ9LCJleHRyYV9kYXRhIjoi5oyJ6ZyA5YaZ77yM55uu5qCH572R5YWz6IO96Kej5p6Q5bCx6KGMIn1dQhIKBm1ldGhvZBIIdHJhbnNmZXIShwEKOwoVd3gtb3JnLmNoYWlubWFrZXIub3JnEAEaIIo+b0nlQpOxQ2C11CGmMXdLJezkW9MMLqDBixJXG+FZEkgwRgIhAILnyAF0L93trn0yty2MvvNDYklobCu/tW7cpV1FkJKRAiEAglzrT4RbRiJN3Fdjzt594INctBLUriQsZUyQfmpWNVsi1wUSsgUSB3N1Y2Nlc3MaB1N1Y2Nlc3Mgk4sBKpkFCgR0ZXN0EkAxNmZiNmQ1ZDk4Y2NkOTRiY2E1MmZkZmMwNzIxODI2NTY3ODNkZTMxMTZjMzQzMGNhYjQ4NjMzOTcyZjFhNjExGghlcmMyMF8wMSIDMS4wKr8EGgtmaXJzdF9ldmVudCILZmlyc3RfZXZlbnQqkAMKATESBmNoYWluMxoIZXJjMjBfMDIiD2ludm9rZV9jb250cmFjdDJoeyJtZXRob2QiOiJtaW50ZXIiLCJ0byI6ImNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjUiLCJ2YWx1ZSI6IjEwIn06J+aMiemcgOWGme+8jOebruagh+e9keWFs+iDveino+aekOWwseihjEKPARIGY2hhaW4zGghlcmMyMF8wMiIPaW52b2tlX2NvbnRyYWN0Kmp7Im1ldGhvZCI6InRyYW5zZmVyIiwidG8iOiIwMWRlZDFhODE4MDQ4MTJiN2Y1MzIwYmM3M2EyNThlYzc0ZTY1MDg5MmU4NDEyMjJjMjI0MDVlNDYyYjY3MGRjIiwidmFsdWUiOiIxMCJ9SkMSBmNoYWluMxoIZXJjMjBfMDIiD2ludm9rZV9jb250cmFjdCoeeyJtZXRob2QiOiJidXJuIiwidmFsdWUiOiIxMCJ9SgBSjQESBmNoYWluMhoIZXJjMjBfMDEiD2ludm9rZV9jb250cmFjdCpoeyJtZXRob2QiOiJtaW50ZXIiLCJ0byI6IjAxZGVkMWE4MTgwNDgxMmI3ZjUzMjBiYzczYTI1OGVjNzRlNjUwODkyZTg0MTIyMmMyMjQwNWU0NjJiNjcwZGMiLCJ2YWx1ZSI6IjIwIn0aIBjZEokbj2Mtb2FFRI86CFg5J3aex93sjNPgaTcZkmHPEAUooMHUlQY=\"}","block_height":"5"}},"cross_chain_tx_content":[{"tx_content":{"tx_id":"30cd958fcf0f454b86326464ce438386e8e992490068484fa3f2912f2f403f04","tx":"CtIBCgZjaGFpbjMaQDMwY2Q5NThmY2YwZjQ1NGI4NjMyNjQ2NGNlNDM4Mzg2ZThlOTkyNDkwMDY4NDg0ZmEzZjI5MTJmMmY0MDNmMDQgocHUlQYyCGVyYzIwXzAyOg9pbnZva2VfY29udHJhY3RCEAoGbWV0aG9kEgZtaW50ZXJCRgoCdG8SQGNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjVCCwoFdmFsdWUSAjEwEnMKKAoVd3gtb3JnLmNoYWlubWFrZXIub3JnEAQaDW15X2NlcnRfYWxpYXMSRzBFAiEAtEEjLJGtyB5i47nzzf8aA+0wuU1szdMSgAn+goBuSIgCIGH7cSosmBJIOLNsoo6CAsrWX07/6HbwjS19kQ50MNypIjASDBoHU3VjY2VzcyDXZhogmNDYbG73ck3nVn4/MR4DtU6N6Yi1ISrIBzoKOratOuA=","gateway_id":"1","chain_id":"chain3","tx_prove":"{\"hash_array\":\"WyJWcENoTmJKZVBBT2kwbzJOb3RkSm9JSjhmbVlsbm5UQUtUQ3E2czErRUhnPSJd\",\"hash_type\":\"U0hBMjU2\",\"tx_byte\":\"CvwCCtIBCgZjaGFpbjMaQDMwY2Q5NThmY2YwZjQ1NGI4NjMyNjQ2NGNlNDM4Mzg2ZThlOTkyNDkwMDY4NDg0ZmEzZjI5MTJmMmY0MDNmMDQgocHUlQYyCGVyYzIwXzAyOg9pbnZva2VfY29udHJhY3RCEAoGbWV0aG9kEgZtaW50ZXJCRgoCdG8SQGNkNDFmMjhmMzllMzdmOGRkZTUyZTYyMmUwZWEwZGVjODExZjJhZDNlOTNjNDc5ZjY2MzI1NTQ0ZDUzMDI4YjVCCwoFdmFsdWUSAjEwEnMKKAoVd3gtb3JnLmNoYWlubWFrZXIub3JnEAQaDW15X2NlcnRfYWxpYXMSRzBFAiEAtEEjLJGtyB5i47nzzf8aA+0wuU1szdMSgAn+goBuSIgCIGH7cSosmBJIOLNsoo6CAsrWX07/6HbwjS19kQ50MNypIjASDBoHU3VjY2VzcyDXZhogmNDYbG73ck3nVn4/MR4DtU6N6Yi1ISrIBzoKOratOuAQBCiiwdSVBg==\"}","block_height":"4"}}],"cross_chain_result":true,"gateway_confirm_result":[{"message":"GATEWAY_SUCCESS"}],"state":"CONFIRM_END","confirm_info":{},"cancel_info":{"chain_id":"chain2","contract_name":"erc20_01","method":"invoke_contract","parameter":"{\"method\":\"minter\",\"to\":\"01ded1a81804812b7f5320bc73a258ec74e650892e841222c22405e462b670dc\",\"value\":\"20\"}"},"confirm_result":{"message":"GATEWAY_SUCCESS"},"timeout":"300"}],"page_info":{"page_size":"10","page_number":"1","total_count":"1","limit":"1"},"message":"GATEWAY_SUCCESS"}
```

## 3. 文件说明

```
├── README.md #本文档
├── chainmaker-go-run                                     #chainmaker执行目录
│   ├── chainmaker-v2.2.0-wx-org.chainmaker.org           #chainmaker节点部署目录
│   │   ├── bin                                           #chainmaker节点可执行文件目录
│   │   ├── config
│   │   │   └── wx-org.chainmaker.org                     #证书目录
│   │   │       ├── chainconfig                           #创世块配置目录
│   │   │       ├── chainmaker.yml                        #链配置
│   │   │       └── log.yml                               #日志配置
│   ├── contract                                          #跨链合约目录
│   │   ├── erc20_01.7z                                   #erc20合约1，在本项目中会自动安装到chain2中
│   │   ├── erc20_02.7z                                   #erc20合约2，在本项目中会自动安装到chain3中
│   │   ├── erc20_03.7z                                   #erc20合约3，在本项目中会自动安装到chain4中
│   │   └── main.go.erc20                                 #erc20合约源码
│   ├── invoke_1_cross_1.sh                               #调用chain2中的erc20合约1，发起跨链转账请求(1对1跨链)
│   ├── invoke_1_cross_2.sh                               #调用chain2中的erc20合约1，发起跨链转账请求（1对2跨链）
│   ├── query_1_cross_1.sh                                #查询chain2中的erc20合约1和chain3中的erc20合约2，查看账户余额(1对1跨链)
│   ├── query_1_cross_2.sh                                #查询chain2中的erc20合约1、chain3中的erc20合约2、chain4中的erc20合约3，查看账户余额(1对2跨链)
│   ├── sdk_config_chain1.yml                             #chain1 sdk配置文件
│   ├── sdk_config_chain2.yml                             #chain2 sdk配置文件
│   ├── sdk_config_chain2_client1.yml                     #chain2 client 1 sdk配置文件,
│   ├── sdk_config_chain3.yml                             #chain3 sdk配置文件
│   ├── sdk_config_chain3_client1.yml                     #chain3 client 1 sdk配置文件,
│   ├── sdk_config_chain4.yml                             #chain4 sdk配置文件
│   ├── sdk_config_chain4_client1.yml                     #chain4 client 1 sdk配置文件,
│   └── start.sh                                          #启动chainmaker，并初始化跨链合约
├── clean.sh                                              #停止所有服务，并清理环境
├── run_test_1_cross_1.sh                                 #执行测试脚本(1对1跨链)
├── run_test_1_cross_2.sh                                 #执行测试脚本(1对2跨链)
├── start_1_cross_1.sh                                    #部署环境(1对1跨链)
├── start_1_cross_2.sh                                    #部署环境(1对2跨链)
├── stop.sh                                               #停止所有服务，不删除数据
├── tcip-chainmaker-1-run                                 #tcip-chainmaker执行目录
│   ├── config                                            #配置文件目录
│   │   ├── cert
│   │   │   ├── client                                    #client证书目录
│   │   │   └── server                                    #服务端证书目录
│   │   ├── certs                                         #chainmaker证书
│   │   ├── sdk_config_chain2.yml                         #chain2 sdk配置文件
│   │   └── tcip_chainmaker.yml                           #tcip-chainmaker-1配置文件
│   ├── contract                                          #spv合约目录
│   │   ├── main.spv.go.txt                               #spv合约源码
│   │   └── spv0chain2.7z                                 #spv合约
│   ├── chain_config.json                                 #链信息配置
│   ├── event_1_cross_1.json                              #1对1跨链事件注册
│   ├── event_1_cross_2.json                              #1对2跨链事件注册
│   ├── switch.sh                                         #关闭启动脚本，必须在本目录下执行，switch.sh up启动，./switch.sh down关闭
│   └── start.sh                                          #tcip-chainmaker-1启动脚本
├── tcip-chainmaker-2-run                                 #该目录下的配置与tcip-chainmaker-1-run目录下的结构相同
├── tcip-relayer-1-run                                     #tcip-relayer执行目录
│   ├── config                                            #配置文件目录
│   │   ├── cert
│   │   │   ├── client                                    #client证书目录
│   │   │   └── server                                    #服务端证书目录
│   │   ├── certs                                         #chainmaker证书
│   │   ├── sdk_config_chain1.yml                         #chain1 sdk配置文件
│   │   └── tcip_relayer.yml                              #tcip-relayer配置文件
│   ├── contract                                          #跨链管理合约目录
│   │   ├── cross_chain_manager.7z                        #跨链管理合约
│   │   └── main.go.txt                                   #跨链管理合约源码
│   ├── chain_config.json                                 #链信息配置
│   ├── switch.sh                                         #关闭启动脚本，必须在本目录下执行，switch.sh up启动，./switch.sh down关闭
│   └── start.sh                                          #tcip-relayer启动脚本
└── tcip-relayer-2-run                                    #该目录下的配置与tcip-relayer-1-run目录下的结构相同
```